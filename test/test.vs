#version 330 core

layout (location = 0) in vec4 aPos;

out vec2 TexCoords;

void main() {
    TexCoords = aPos.zw;
    gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);
}
