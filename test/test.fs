#version 330 core

int vec2 TexCoords;
out vec4 FragColor;

uniform sampler2D image;
uniform vec3 color;

void main() {
    FragColor = texture(image, TexCoords) * vec4(1.0, 1.0, 1.0, 1.0);
}
