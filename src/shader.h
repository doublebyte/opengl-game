#pragma once

#include <string_view>

#include <glad/glad.h>

class Shader {
public:
    Shader(const std::string_view vertexPath, const std::string_view fragmentPath, const std::string_view geometryPath = "");
    ~Shader() = default;

    void Use() const;
    Shader const &Get() const;
    GLuint inline GetProgramID() const { return programID_; };

private:
    GLuint programID_;

    void GenerateProgram(const std::string_view vertexPath, const std::string_view fragmentPath);
};
