#include "util.h"

#include <fstream>
#include <sstream>

#include <log4cc.h>

bool ReadFile(const std::string_view path, std::string &code) {
    std::ifstream file;
    file.exceptions(std::ifstream::failbit | std::ifstream::badbit);

    try {
        file.open(path.data());
        std::stringstream fileStream;
        fileStream << file.rdbuf();
        code.clear();
        code = fileStream.str();
    } catch (std::ifstream::failure &error) {
        LOGE("failed to read shader file", error.what());
        file.close();
        return false;
    }
    file.close();
    return true;
}
