#include <string>
#include <string_view>

bool ReadFile(const std::string_view path, std::string &code);
