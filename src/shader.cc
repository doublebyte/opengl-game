#include "shader.h"

#include <array>
#include <cstddef>
#include <string>

#include <log4cc.h>

#include "util.h"

namespace {
enum CheckType {
    VERTEX = 0x1,
    FRAGMENT = 0x2,
    PROGRAM = 0x4,
};

bool CheckCompile(GLuint shader, CheckType type) {
    GLint success;
    std::array<GLchar, 1024> infoLog;

    if ((type & VERTEX) == VERTEX) {
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if (!success) {
            glGetShaderInfoLog(shader, infoLog.size(), nullptr, infoLog.data());
            LOGE("vertex generate failed:", infoLog.data());
            return false;
        }
    }
    if ((type & FRAGMENT) == FRAGMENT) {
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if (!success) {
            glGetShaderInfoLog(shader, infoLog.size(), nullptr, infoLog.data());
            LOGE("fragment generate failed:", infoLog.data());
            return false;
        }
    }
    if ((type & PROGRAM) == PROGRAM) {
        glGetShaderiv(shader, GL_LINK_STATUS, &success);
        if (!success) {
            glGetProgramInfoLog(shader, infoLog.size(), nullptr, infoLog.data());
            LOGE("program generate failed:", infoLog.data());
            return false;
        }
    }
    return true;
}
} // namespace

Shader::Shader(const std::string_view vertexPath, const std::string_view fragmentPath, const std::string_view geometryPath) {
    GenerateProgram(vertexPath, fragmentPath);
}

void Shader::GenerateProgram(const std::string_view vertexPath, const std::string_view fragmentPath) {
    LOGD("vertexPath", vertexPath, "fragmentPath", fragmentPath);
    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    std::string code;

    if (!ReadFile(vertexPath, code)) {
        LOGE("read vertex failed");
        return;
    }
    const char *vertexCode = code.c_str();
    glShaderSource(vertexShader, 1, &vertexCode, nullptr);
    glCompileShader(vertexShader);
    if (!CheckCompile(vertexShader, VERTEX)) {
        LOGE("vertexShader checkCompile failure");
        glDeleteShader(vertexShader);
        return;
    }

    if (!ReadFile(fragmentPath, code)) {
        LOGE("read fragment failed");
        return;
    }
    const char *fragmentCode = code.c_str();
    glShaderSource(fragmentShader, 1, &fragmentCode, nullptr);
    glCompileShader(fragmentShader);
    if (!CheckCompile(fragmentShader, FRAGMENT)) {
        LOGE("fragmentShader checkCompile failure");
        return;
    }

    programID_ = glCreateProgram();
    glAttachShader(programID_, vertexShader);
    glAttachShader(programID_, fragmentShader);
    glLinkProgram(programID_);
    if (!CheckCompile(programID_, PROGRAM)) {
        LOGE("program checkCompile failure");
    }

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
}
