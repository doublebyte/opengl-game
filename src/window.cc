#include "window.h"

#include <log4cc.h>

namespace {

void ResizeCallback(GLFWwindow *window, int width, int height) {
    glViewport(0, 0, width, height);
}

void inline CheckExit(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

} // namespace

Window::Window(int width, int height, std::string_view title) : width_(width), height_(height), title_(title) {}

bool Window::Create() {
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    window_ = glfwCreateWindow(width_, height_, title_.data(), nullptr, nullptr);
    if (window_ == nullptr) {
        LOGE("glfwCreateWindow failed");
        return false;
    }
    glfwMakeContextCurrent(window_);
    glfwSetFramebufferSizeCallback(window_, ResizeCallback);

    if (!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress))) {
        LOGE("gladLoadGLLoader failed");
        return false;
    }
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    return true;
}

void Window::Run() {
    while (!glfwWindowShouldClose(window_)) {
        glClearColor(0.0, 0.0, 0.0, 0.0);
        glClear(GL_COLOR_BUFFER_BIT);

        if (onDraw_ != nullptr) {
            onDraw_();
        }
        if (onInput_ != nullptr) {
            onInput_(window_);
        }

        glfwSwapBuffers(window_);
        glfwPollEvents();
        CheckExit(window_);
    }
}

void Window::Destroy() {
    glfwTerminate();
}

void Window::SetDrawFunc(const DrawFunc &onDraw) {
    onDraw_ = onDraw;
}

void Window::SetInputFunc(const InputFunc &onInput) {
    onInput_ = onInput;
}

void Window::SetKeyFunc(const GLFWkeyfun keyCallback) {
    glfwSetKeyCallback(window_, keyCallback);
}
