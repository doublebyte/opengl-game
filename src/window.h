/*
 * Copyright (c) 2023 DoubleByte.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#pragma once

#include <functional>
#include <string_view>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

using DrawFunc = std::function<void()>;
using InputFunc = std::function<void(const GLFWwindow *window)>;

class Window {
public:
    Window(int width, int height, std::string_view title);
    ~Window() = default;

    bool Create();
    void Run();
    void Destroy();
    void SetDrawFunc(const DrawFunc &onDraw);
    void SetInputFunc(const InputFunc &onInput);

    void SetKeyFunc(const GLFWkeyfun keyCallback);

private:
    GLFWwindow *window_ = nullptr;
    DrawFunc onDraw_ = nullptr;
    InputFunc onInput_ = nullptr;

    int width_;
    int height_;
    std::string_view title_;
};
