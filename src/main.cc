#include <memory>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <log4cc.h>

#include "window.h"

namespace {
void OnKey(GLFWwindow *window, int key, int scancode, int action, int mode) {
    LOGI("key", key);
}
} // namespace

int main() {
    std::unique_ptr<log4cc::Logger> logger = std::make_unique<log4cc::Logger>();
    LOGI("hello opengl game");

    std::unique_ptr<Window> window = std::make_unique<Window>(800, 600, "Backout");
    window->Create();
    window->SetInputFunc([](const auto window) {
        // LOGI("render");
    });
    window->SetKeyFunc(OnKey);
    window->Run();
    window->Destroy();

    return 0;
}
